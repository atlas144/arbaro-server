package com.arbaro.arbaroserver;

import com.arbaro.model.dto.GarbageAmount;
import com.arbaro.model.dto.GarbagePlace;
import com.arbaro.model.dto.GarbageType;
import java.sql.SQLException;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GarbageController {
    
    private final GarbageService garbageService;

    public GarbageController() throws SQLException {
        garbageService = new GarbageService();
    }
    
    @GetMapping("/api/garbage/type/all")
    public List<GarbageType> getAllGarbageTypes() {
        return garbageService.getAllGarbageTypes();
    }
    
    @GetMapping("/api/garbage/amount/all")
    public List<GarbageAmount> getAllGarbageAmount() {
        return garbageService.getAllGarbageAmount();
    }
    
    @GetMapping("/api/garbage/place/all")
    public List<GarbagePlace> getAllGarbagePlaces() throws SQLException {
        return garbageService.getAllGarbagePlaces();
    }
    
    @PostMapping("/api/garbage/place")
    public void postGarbagePlace(@RequestBody GarbagePlace garbagePlace) throws SQLException {
        garbageService.postGarbagePlace(garbagePlace.getLat(), garbagePlace.getLon(), garbagePlace.getGarbageAmount(), garbagePlace.getGarbageTypes());
    }
}
