package com.arbaro.model.dto;

public class BinPlace extends Place {

    private final String name;
    private final boolean paper;
    private final boolean plastic;
    private final boolean glass;

    public BinPlace(double lat, double lon, String name, boolean paper, boolean plastic, boolean glass) {
        super(lat, lon);

        this.name = name;
        this.paper = paper;
        this.plastic = plastic;
        this.glass = glass;
    }

    public String getName() {
        return name;
    }

    public boolean containsPaper() {
        return paper;
    }

    public boolean containsPlastic() {
        return plastic;
    }

    public boolean containsGlass() {
        return glass;
    }

}
