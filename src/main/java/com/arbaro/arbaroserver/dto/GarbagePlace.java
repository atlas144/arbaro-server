package com.arbaro.model.dto;

import java.util.List;

public class GarbagePlace extends Place {

    private final int id;
    private final List<GarbageType> garbageTypes;
    private final GarbageAmount garbageAmount;

    public GarbagePlace(int id, double lat, double lon, List<GarbageType> garbageTypes, GarbageAmount garbageAmount) {
        super(lat, lon);

        this.id = id;
        this.garbageTypes = garbageTypes;
        this.garbageAmount = garbageAmount;
    }

    public int getId() {
        return id;
    }

    public List<GarbageType> getGarbageTypes() {
        return garbageTypes;
    }

    public GarbageAmount getGarbageAmount() {
        return garbageAmount;
    }

}
