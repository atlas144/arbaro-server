package com.arbaro.model.dto;

public abstract class Place {

    private final double lat;
    private final double lon;

    public Place(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

}
