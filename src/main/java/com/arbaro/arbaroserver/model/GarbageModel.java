package com.arbaro.arbaroserver.model;

import com.arbaro.arbaroserver.model.db.GarbageDBHandler;
import com.arbaro.model.dto.GarbageAmount;
import com.arbaro.model.dto.GarbagePlace;
import com.arbaro.model.dto.GarbageType;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GarbageModel {

    private static final String HOST = "192.168.100.96";
    private static final short PORT = 3306;
    private static final String DB_NAME = "arbaro";

    private final GarbageDBHandler garbageDBHandler;

    private final HashMap<Integer, GarbageType> garbageTypeHashMap;
    private final HashMap<Integer, GarbageAmount> garbageAmountHashMap;

    public GarbageModel() throws SQLException {
        garbageDBHandler = new GarbageDBHandler(HOST, PORT, DB_NAME);

        garbageTypeHashMap = garbageDBHandler.loadGarbageTypes();
        garbageAmountHashMap = garbageDBHandler.loadGarbageAmount();
    }

    public List<GarbagePlace> getAllGarbagePlaces() throws SQLException {
        return garbageDBHandler.loadAllGarbagePlaces(garbageTypeHashMap, garbageAmountHashMap);
    }

    public List<GarbageType> getAllGarbageTypes() {
        return new ArrayList<>(garbageTypeHashMap.values());
    }

    public List<GarbageAmount> getAllGarbageAmount() {
        return new ArrayList<>(garbageAmountHashMap.values());
    }

    public void addGarbagePlace(double lat, double lon, GarbageAmount garbageAmount, List<GarbageType> garbageTypeList) throws SQLException {
        garbageDBHandler.insertGarbagePlace(lat, lon, garbageAmount, garbageTypeList);
    }

}
