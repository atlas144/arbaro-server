package com.arbaro.arbaroserver.model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class DBHandler {

    protected final Connection connection;

    public DBHandler(String host, short port, String dbName) throws SQLException {
        Properties connConfig = new Properties();
        connConfig.setProperty("user", "franta");
        connConfig.setProperty("password", "lala");
        
        connection = DriverManager.getConnection(String.format("jdbc:mariadb://%s:%s/%s", host, port, dbName), connConfig);
    }

}
