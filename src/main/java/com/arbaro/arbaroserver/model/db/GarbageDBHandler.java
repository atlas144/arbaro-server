package com.arbaro.arbaroserver.model.db;

import com.arbaro.model.dto.GarbageAmount;
import com.arbaro.model.dto.GarbagePlace;
import com.arbaro.model.dto.GarbageType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GarbageDBHandler extends DBHandler {

    public GarbageDBHandler(String host, short port, String dbName) throws SQLException {
        super(host, port, dbName);
    }

    public HashMap<Integer, GarbageType> loadGarbageTypes() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM garbage_types");

        HashMap<Integer, GarbageType> garbageTypesMap = new HashMap<>();

        while(result.next()) {
            int id = result.getInt("id");

            garbageTypesMap.put(id, new GarbageType(
                    id,
                    result.getString("name")
            ));
        }

        return garbageTypesMap;
    }

    public HashMap<Integer, GarbageAmount> loadGarbageAmount() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM garbage_amount");

        HashMap<Integer, GarbageAmount> garbageTypesMap = new HashMap<>();

        while(result.next()) {
            int id = result.getInt("id");

            garbageTypesMap.put(id, new GarbageAmount(
                    id,
                    result.getString("name"),
                    result.getString("description")
            ));
        }

        return garbageTypesMap;
    }

    public List<GarbagePlace> loadAllGarbagePlaces(HashMap<Integer, GarbageType> garbageTypeHashMap, HashMap<Integer, GarbageAmount> garbageAmountHashMap) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM garbage_places1");

        List<GarbagePlace> garbagePlaces = new ArrayList<>();

        while(result.next()) {
            int id = result.getInt("id");
            List<GarbageType> garbageTypes = new ArrayList<>();
            PreparedStatement typesStatement = connection.prepareStatement("SELECT garbage_types_id FROM garbage_places_types WHERE garbage_places_id = ?");

            typesStatement.setInt(1, id);

            ResultSet typesResult = typesStatement.executeQuery();

            while (typesResult.next()) {
                garbageTypes.add(garbageTypeHashMap.get(typesResult.getInt("garbage_types_id")));
            }

            garbagePlaces.add(new GarbagePlace(
                id,
                result.getFloat("lat"),
                result.getFloat("lon"),
                garbageTypes,
                garbageAmountHashMap.get(result.getInt("amount"))
            ));
        }

        return garbagePlaces;
    }

    public void insertGarbagePlace(double lat, double lon, GarbageAmount garbageAmount, List<GarbageType> garbageTypes) throws SQLException {
        PreparedStatement placeStatement = connection.prepareStatement("INSERT INTO garbage_places (lat, lon, amount) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        placeStatement.setDouble(1, lat);
        placeStatement.setDouble(2, lon);
        placeStatement.setInt(3, garbageAmount.getId());

        int placeId = placeStatement.executeUpdate();

        connection.setAutoCommit(false);

        PreparedStatement typesStatement = connection.prepareStatement("INSERT INTO garbage_places_types (garbage_places_id, garbage_types_id) VALUES (?, ?)");

        for (GarbageType garbageType: garbageTypes) {
            typesStatement.setInt(1, placeId);
            typesStatement.setInt(2, garbageType.getId());
            typesStatement.addBatch();
        }

        typesStatement.executeBatch();
    }

}
