package com.arbaro.arbaroserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArbaroServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArbaroServerApplication.class, args);
    }

}
