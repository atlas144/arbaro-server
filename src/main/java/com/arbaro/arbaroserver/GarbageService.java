package com.arbaro.arbaroserver;

import com.arbaro.arbaroserver.model.GarbageModel;
import com.arbaro.model.dto.GarbageAmount;
import com.arbaro.model.dto.GarbagePlace;
import com.arbaro.model.dto.GarbageType;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author atlas144
 */
public class GarbageService {
    
    private final GarbageModel garbageModel;
    
    public GarbageService() throws SQLException {
        garbageModel = new GarbageModel();
    }
    
    public List<GarbageType> getAllGarbageTypes() {
        return garbageModel.getAllGarbageTypes();
    }
    
    public List<GarbageAmount> getAllGarbageAmount() {
        return garbageModel.getAllGarbageAmount();
    }
    
    public List<GarbagePlace> getAllGarbagePlaces() throws SQLException {
        return garbageModel.getAllGarbagePlaces();
    }
    
    public void postGarbagePlace(double lat, double lon, GarbageAmount garbageAmount, List<GarbageType> garbageTypeList) throws SQLException {
        garbageModel.addGarbagePlace(lat, lon, garbageAmount, garbageTypeList);
    }
    
}
